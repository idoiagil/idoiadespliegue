<?php
session_start();
if (isset($_SESSION['adm'])) {
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Nuevo piso </title>
    </head>
    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
            <?php menuAdmin(); ?>
            <div class="contenedoradmin">
                <div class="sign-up">
                    <div class="form">
                        <h2> Nuevo Piso </h2>
                        <form action="../controlador/vaNuevoPiso.php" method="post" enctype="multipart/form-data">
                            <label>
                                <span> Título </span>
                                <input type="text" name="titulo" id="titulo" required>
                            </label>
                            <label>
                                <span> Número de habitaciones</span>
                                <input type="number" name="habitaciones" id="habitaciones" required>
                            </label>
                            <label>
                                <span> Precio</span>
                                <input type="number" name="precio" id="precio" required>
                            </label>
                            <label>
                                <span> Descripcion </span>
                                <input type="text" name="descripcion" id="descripcion" required>
                            </label>
                            <label>
                                <span>Distancia al Montessori</span>
                                <input type="number" name="distancia" id="distancia" required>
                            </label>
                            <label>
                                <span>Telefono </span>
                                <input type="text" name="telefono" id="telefono" required>
                            </label>
                            <label for="imagen">
                                <span>Foto: </span>
                                <input type="file" name="imagen" size="30" required>
                            </label>
                            <button type="submit" class="submit"> Entrar </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php
} else {
    echo "Usted no es administrador, por lo que no puede entrar en la página";
}
    ?>
    </body>
    </html>