<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Dar de baja </title>
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <?php menuUser(); ?>
            <div class="contenedoradmin">
                <div class="sign-up">
                    <div class="form">
                        Para eliminar la cuenta debe de introducir la contraseña:
                        <form action="../controlador/vuBaja.php" method="post">
                            <label>
                                <span> Contraseña: </span>
                                <input type="password" name="contra" id="contra" required>
                            </label>
                            <button class="submit" type="submit"> Entrar </button>
                        </form>
                    </div>
                </div>
            </div>
        <?php
    } else {
        echo "Usted no es usuario, por lo que no puede entrar en la página.";
    }
        ?>
    </body>

    </html>