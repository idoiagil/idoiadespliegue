<?php
session_start();
if (isset($_SESSION['adm'])) {
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Responder </title>
    </head>
    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
            <?php menuAdmin(); ?>
            <div class="contenedoradmin">
                <?php
                $titulo = $_POST['titulo'];
                $de = $_POST['de'];
                $para = $_POST['para'];
                $asunto = $_POST['asunto'];
                ?>
                <div class="form">
                    <form action="../controlador/vaMensaje.php" method="post">
                        <label>
                            <span><b>Para: </b> <?php echo $para; ?> </span>
                            <input type="hidden" value="<?php echo $para; ?>" name="para">
                        </label>
                        <label>
                            <span><b>De: </b> <?php echo $de; ?> </span>
                            <input type="hidden" value="<?php echo $de; ?>" name="de">
                        </label>
                        <label>
                            <span><b>Asunto: </b></span>
                            <input type="text" value="<?php echo $asunto; ?>" name="asunto">
                        </label>
                        <label>
                            <span><b>Piso: </b> <?php echo $titulo; ?> </span>
                            <input type="hidden" value="<?php echo $titulo; ?>" name="titulo">
                        </label>
                        <label>
                            <span>Mensaje: </span>
                            <textarea name="mensaje" id="mensaje" required></textarea>
                        </label>
                        <button class="submit" type="submit"> Enviar </button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<?php } else { echo "Usted no es un administrador"; } ?>