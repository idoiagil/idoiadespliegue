<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PPV</title>
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link href="../css/estilos.css" rel="stylesheet">
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <div class="form">
                <form action="../controlador/vuMensaje.php" method="post">
                    <label>
                        <span>Asunto: </span>
                        <textarea name="asunto" id="asunto" required></textarea>
                    </label>
                    <label>
                        <span>Mensaje: </span>
                        <textarea name="mensaje" id="mensaje" required></textarea>
                    </label>
                    <input type="hidden" name="de" value="<?php echo $_SESSION['nick'] ?>" required>
                    <input type="hidden" name="titulo" value="<?php echo $_SESSION['titulo'] ?>" required>
                    <input type="hidden" name="para" value="adm" required>
                    <button class="submit" type="submit"> Enviar </button>
                </form>
            </div>
        </div>
    </body>
<?php } else {
    echo "usted no es un usuario";
} ?>

    </html>