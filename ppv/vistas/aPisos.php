<?php
session_start();
if (isset($_SESSION['adm'])) {
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Pisos </title>
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
            <?php menuAdmin(); ?>
            <div class="contenedoradmin">
                <a href="aNuevoPiso.php"> Añadir piso </a>
                <a href="aModificarPiso.php"> Modificar piso</a>
                <a href="aEliminarPiso.php"> Eliminar piso </a>
            </div>
        </div>
    <?php
} else {
    echo "Usted no es administrador, por lo que no puede entrar en la página";
}
    ?>
    </body>
    </html>