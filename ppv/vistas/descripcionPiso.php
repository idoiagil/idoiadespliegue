
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Descripción Piso</title>
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">

    </head>

    <body>

        <?php include("zlibreria.php"); ?>
        <?php navbar(); ?>
        <br><br><br>
        <div id="informacion" style="display: block; margin-bottom: 100px;">
            <?php
            $id = $_POST['id'];
            $titulo = $_POST['titulo'];
            $foto = $_POST['foto'];
            $precio = $_POST['precio'];
            $habitaciones = $_POST['habitaciones'];
            $distancia = $_POST['distancia'];
            $telefono = $_POST['telefono'];
            $descripcion = $_POST['descripcion'];
            $nick = $_SESSION['nick'];
            include "../modelo/piso.php";
            $piso = new Piso();
            if (isset($_SESSION['nick']) || isset($_SESSION['adm'])) {
                $piso->muestramePiso($id);
                echo '<div style="display: block; margin: 0 auto; width: 63%; ">';
                echo "<img class='d-block w-100' src='../images/" . $_POST['foto'] . "' alt='Third slide'>";
                echo '</div>';
                echo '<div style="display: block; margin: 0 auto; width: 45%; position: relative; left: 100px; top: 50px;">';
                echo "<br><h4>" . $_POST['titulo'] . "</h4><br>";
                echo "<h5>" . $_POST['precio'] . "€</h5><br>";
                echo "<p> <b>Habitaciones:</b> " . $_POST['habitaciones'] . "</p>";
                echo "<p> <b>Distancia:</b> " . $_POST['distancia'] . "</p>";
                echo "<p> <b>Telefono:</b> " . $_POST['telefono'] . "</p>";
                echo "<p><b>Descripción: </b></p>" . $_POST['descripcion'];
                if (isset($_SESSION['nick'])) {
                    echo "<br><br><br>";
                    echo "<p> <h5>Si quieres más información, contacta con nosotros : <a href='uNuevoMensaje.php'> aquí </a></h5></p>";
                }
                echo '<form action="../controlador/descargarPDF.php" method="post">';
                echo '<input type="hidden" name="id" value="' . $id . '">';
                echo ' <input type="hidden" name="titulo" value="' . $titulo . '">';
                echo '<input type="hidden" name="foto" value="' . $foto . '">';
                echo '<input type="hidden" name="precio" value="' . $precio . '">';
                echo '<input type="hidden" name="habitaciones" value="' . $habitaciones . '">';
                echo '<input type="hidden" name="distancia" value="' . $distancia . '">';
                echo '<input type="hidden" name="telefono" value="' . $telefono . '">';
                echo '<input type="hidden" name="descripcion" value="' . $descripcion . '">';
                echo '<label>';
                echo '<input type="submit" value="Descargar PDF">';
                echo '</label>';
                echo '</form>';
                echo '<form action="../controlador/vuFavoritos.php" method="post">';
                echo '<input type="hidden" name="nick" value="' . $nick . '">';
                echo '<input type="hidden" name="idPiso" value="' . $titulo . '">';
                echo '<label>';
                echo '<input type="submit" value="Añadir a favoritos">';
                echo '</label>';
                echo '</form>';
                echo '</div>';
            } else {
                header("Location: uRegistro.php");
            }
            ?>

        </div>
    </body>
    </html>