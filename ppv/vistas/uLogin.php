<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Iniciar Sesión </title>
    <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
<?php include("zlibreria.php"); ?>
<?php navbar(); ?>
<div class="principal">
<div class="cont">
    <div class="form">
        <h2>Inicia sesión </h2>
        <form action="../controlador/vuLogin.php" method="post">
            <label>
            <span> Nick </span>
            <input type="text" name="nick" required>
            </label>
            <label>
            <span>Contraseña</span>
            <input type="password" name="contra" required>
            </label>
            <button class="submit" type="submit"> Entrar </button>
            <button type="button" class="forgot-pass">¿Te has olvidado de la contraseña?</button>
            <a class="regis" href="uRegistro.php"> Registrate!</a>
        </form>
    </div>
</div>
</div>


</body>
</html>