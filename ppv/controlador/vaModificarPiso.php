<?php
include("../modelo/piso.php");
$piso = new Piso();

$titulo = $_POST['titulo'];
$habitaciones = intval($_POST['habitaciones']);
$precio = intval($_POST['precio']);
$descripcion = $_POST['descripcion'];
$distancia = intval($_POST['distancia']);
$telefono = $_POST['telefono'];
$nombreimg = $_FILES['imagen']['name'];
$imagenhidden = $_POST['imagenhidden'];
$extension = pathinfo($nombreimg, PATHINFO_EXTENSION);
$tamano = $_FILES['imagen']['size'];

if ($tamano <= 30000000) {
   if ($extension == "jpeg" || $extension == "jpg" || $extension == "png" || $extension == "gif") {
      $directorio = '/proyecto/images/';
      move_uploaded_file($_FILES['imagen']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $directorio . $nombreimg);
   } else {
      echo "Solo se pueden subir imagenes de tipo jpeg, jpg, png o gif";
   }
} else {
   echo "El tamaño es demasiado grande";
}
if ($nombreimg == "") {
   $nombreimg = $imagenhidden;
}

if ($piso->modPiso($titulo, $habitaciones, $precio, $descripcion, $distancia, $telefono, $nombreimg)) {
   header("Location: ../vistas/aModificarPiso.php");
} else {
   header("Location: ../vistas/aModificarPiso.php");
}
