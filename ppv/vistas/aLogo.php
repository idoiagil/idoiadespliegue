    <?php
    session_start();
    if (isset($_SESSION['adm'])) {
    ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
            <link rel="stylesheet" href="../css/estilos.css">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title> Cambiar logo </title>
        </head>
        <body>
            <?php include("zlibreria.php"); ?>
            <?php navbarAdmin(); ?>
            <div class="bd">
                <?php menuAdmin(); ?>
                <div class="contenedoradmin">
                    <h1> Introduce nuevo logo: </h1>
                    <form action="../controlador/vaLogo.php" method="post" enctype="multipart/form-data">
                        <label for="logo">
                            <span>Foto: </span>
                            <input type="file" name="logo" size="30" required>
                        </label>
                        <button type="submit" class="submit"> Entrar </button>
                    </form>
                </div>
            </div>
        <?php
    } else {
        echo "Usted no es administrador, por lo que no puede entrar en la página";
    }
        ?>
        </body>
        </html>