-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2020 a las 13:20:05
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ppv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `adm` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `adm`, `contra`) VALUES
(1, 'pepe', 'pepe'),
(6, 'patri', 'patri'),
(7, 'pepa', 'pepa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `id` int(11) NOT NULL,
  `nick` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idPiso` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`id`, `nick`, `idPiso`) VALUES
(16, 'i', 'Piso en venta en calle de Miguel Servet'),
(17, 'i', 'Piso en calle Marqués San Felices, Miralbueno, Zaragoza'),
(18, 'i', 'Piso en venta en calle Franco y Lopez, 18'),
(19, 'juan', 'Piso en venta en calle de Miguel Servet'),
(20, 'juan', 'Piso en calle Marqués San Felices, Miralbueno, Zaragoza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `logo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `para` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `de` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` text COLLATE utf8_spanish_ci NOT NULL,
  `asunto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `leido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `para`, `de`, `titulo`, `mensaje`, `asunto`, `leido`) VALUES
(15, 'i', 'adm', 'Piso en Gran Vía, 23, Paseo Sagasta', 'Esto es un mensaje de respuesta ejemplo', 'gfdgfdg', 1),
(26, 'juan', 'adm', 'Piso en venta en calle de Miguel Servet', 'prueba respuesta nueva', 'Hola ', 1),
(27, 'juan', 'adm', 'Piso en venta en calle de Miguel Servet', 'prueba respuesta 2', 'Hola ', 1),
(28, 'adm', 'juan', 'Piso en venta en calle de Miguel Servet', 'Prueba responder despues de cambiar la funcion', 'Hola ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `habitaciones` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `distancia` double NOT NULL,
  `telefono` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`id`, `titulo`, `habitaciones`, `precio`, `descripcion`, `distancia`, `telefono`, `foto`) VALUES
(8, 'Piso en calle Marqués San Felices, Miralbueno, Zaragoza', 3, 245001, 'Piso de obra nueva. 71m² construidos. Tiene 3 habitaciones, 2 baños y terraza. Cuenta, además, con una certificación energética de proyecto (IPE no indicado). En cuanto al edificio, cuenta con ascensor y piscina. Primera planta exterior.', 11, '876780346', 'p3.png'),
(9, 'Piso en venta en calle Franco y Lopez, 18', 3, 124000, 'En el número 18 de la calle Franco y López, en el barrio de Delicias, cerca del Distrito Universidad, del centro comercial Aragonia y de los hospitales Miguel Servet y del Clínico Universitario Lozano. Piso de 59m² constuidos, 43m² útiles, 1 baño. Tiene terraza, plaza de garaje incluida en el precio, promoción de obra nueva, certificación enerética de proyecto. Bajo exterior, con ascensor.', 1, '876870759', 'p2.png'),
(10, 'Piso en Gran Vía, 23, Paseo Sagasta', 2, 165000, 'Piso de 87m², 80² útiles muy bien distribuidos en dos dormitorios de 12,50 m² y 9,34m², con dos cuartos de baños completos con plato de ducha, el salón más cocina tiene 29.72m², con acceso a una terraza comunitaria. Es un bajo, el piso se encuentra totalmente reformado para poder entrar a vivir. ', 0, '876871503', 'p3.png'),
(11, 'Piso en venta en calle de Miguel Servet', 3, 210000, '108m² construidos, 96m² útiles. Cuenta con 3 habitaciones, 2 baños y terraza. Este piso se encuentra en Calle de Miguel Servet, 50008, Zaragoza, Zaragoza, situado en el distrito de Las Fuentes, en la planta 5. Es un piso, construido en el año 1968, que tiene 108 m2 de los cuales 96 m2 son útiles y dispone de 3 habitaciones y 2 baños. Piso en excelentes condiciones, para entrar a vivir. Cuenta con una terraza de 12 m2. Orientación noreste con orientación exterior. Calefacción central. La cocina y los baños se quedarían como en fotos. El piso está ubicado rodeado de paradas de bus y de comercios. Los gastos de comunidad son de 115€ al mes. Suelo de tarima y ventanas de aluminio. Según estudio personal de solvencia, posibilidad del 100% de financiación. ', 5, '876274618', 'p4.png'),
(12, 'Piso en calle del Coso', 3, 349000, 'Gran piso señorial, consta de 190 m2 construidos de los cuales 166 m2 son útiles, están distribuidos en gran entrada, salón de 30 m2, una sala de estar de 27 m2, cinco dormitorios, dos baños y la cocina con salida a una terraza 8 m2, dispone también de otra galería de unos 3m2 que sirve como desahogo porque está cerrada con aluminio. Tiene muchas posibilidades de reforma.  Los suelos son de parquet y de terrazo, la calefacción central y agua caliente central con contador.  Muy buena finca, adaptada para la movilidad reducida, con cuatro ascensores y portero físico.  Inmejorable ubicación, en el corazón de la ciudad, calle Coso esquina con calle Blancas, frente al Teatro Principal, junto a la Plaza de España y Paseo Independencia, y el Casco Histórico, zona con todo tipo de servicios, comercios, colegios, transporte, bus, tranvía, bares, restaurantes, etc.', 5, '876784135', 'p5.png'),
(13, 'Piso en calle Teniente Coronel Valenzuela, Zaragoza', 6, 670000, '321m², 256m² útiles. Tiene 6 habitaciones y 3 baños, balcón, armarios empotraos, trastero. Planta 4º con ascensor y aire acondicionado.', 5, '876275274', 'p6.png'),
(14, 'Piso en la calle del Coso, 88', 2, 178500, 'Apartamento muy bonito a estrenar, junto a Teatro Romano y Plaza España. Directo de propiedad. Diseñado por estudio profesional. Exterior, orientación sur. Salón - comedor muy amplio, de 22m^2. Cocina acristalada totalmente equipada con mobiliario LUXE y electrodomésticos BOSCH. Dos dormitorios, el principal de 15 m^2 con vestidor. El segundo amplio, de 9m^2. Amplio baño, con cerámica porcelánica Saloni, con ducha con rociador efecto luvia y lavabo con mueble lacado. Galería de 3 m^2 a patio de luces. Parquet en toda la casa. Puertas en laca blanca, ventanas con triple acristalamiento para aislamiento acústico y térmico en ventanales exteriores a Coso. Doble acristalamiento tipo Climalit en dormitorio a patio de luces. Iluminación directa empotrada y ambiental con tiras lumínicas, tipo led. Preinstalación de aire acondicionado. Luminoso. Calefacción comunitaria incluida en cuota comunidad (75e/mes) En pleno centro, edificio de construcción moderna en hormigón, con servicio de conserje de mañanas. Interesados, llamar al 625 66 81 45.', 2, '625667145', 'p7.png'),
(15, 'Piso en la callle Valle de Broto, Grancasa', 5, 210000, 'Piso en Valle de Broto, Edificio KASAN. Son 177 metros construidos y 156 metros útiles, muy bien distribuidos en cinco amplios Dormitorios, con Dos cuartos de Baño, Salón más Comedor de 42 metros útiles, un amplio Hall, Es un piso luminoso y soleado, construido en año 1977. Dispone de dos amplias terrazas de 15m2 y 9m2. Ademas su buena construcción y características permitirán que una vez actualizada disfrute de un hogar de alto Standing. Se el Primero en Llamarnos para ir a Verlo, Llame a DonPiso calle Mariano Barbasan nº4 Tlf 876645373 ( El PVP indicado no incluye impuestos ni gastos de Escritura).', 11, '876871503', 'p8.png'),
(16, 'Piso en calle de Pablo Neruda, Grancasa', 2, 270000, 'VIVIENDA A LA VENTA EN PABLO NERUDA, EN EL ACTUR, CERCA DE TODOS LOS SERVICIOS, PARA ENTRAR A VIVIR', 10, '876276253', 'p20.png'),
(17, 'Piso en Nicolas Guillen, Grancasa', 3, 230000, 'Vivienda de tres dormitorios y salón, amplia cocina y dos baños. Perfecta distribución, con la cocina y el salón a la entrada y sin perder metros en el pasillo. Habitación principal tipo suite, con baño completo y equipado con gran bañera semicircular de hidromasaje. Segundo baño también reformado con bañera pequeña. La cocina es de buen tamaño, con espacio para comer y pequeña galería, perfecta como tendedor y espacio de desahogo. Gran salón, con salida a una terraza exterior a la calle Nicolás Guillén. Armario empotrado de gran tamaño en el recibidor, ideal como gabanero. Calefacción individual de gas y split de aire acondicionado en el salón y en un dormitorio. Finca, sin barreras arquitectónicas, con ascensor. Plaza de garaje y trastero incluidos en el precio, en la misma finca, con acceso directo desde el ascensor. La plaza es para coche grande, de fácil maniobra.', 12, '876871964', 'p17.png'),
(18, 'Piso en Grancasa', 2, 290000, 'Piso precioso totalmente reformado junto a los colegios de Maristas y Sagrado Corazón. Consta de un gran salón, dos dormitorios ( antes eran tres), un gran cocina comedor y baño completo. Terraza exterior muy acogedora y luminosa. Ascensor y calefacción. Plaza de garaje y trastero en la finca. Preciosa reforma. VENGA A VERLO. ( El PVP indicado no incluye impuestos ni gastos de Escritura ).', 11, '876870736', ''),
(19, 'Piso en Mariano Barbasán, Universidad', 3, 140000, 'HA BAJADO DE PRECIO. OCASIÓN. Oportunidad, piso zona universitaria, calle Mariano Barbasan, son 96 metros construidos y 89 metros útiles, para reformar. Su buena construcción y características permitirán que una vez actualizada disfrutes de un bonito hogar, en una buena zona de Zaragoza. Distribuido en salón-comedor de 24 metros, exterior a la misma calle Mariano Barbasan, dos dormitorios de 15 m2 y 10 m2, cocina de 7,90m2 gran ventanal a patio interior, y un cuarto de baño de 5m2, con ventana. Ubicado en zona Fernando Católico y estación Goya. Construido en 1950, muy luminoso y soleado, exterior. No deje de pasar esta Oportunidad, y llámenos a DonPiso calle Mariano Barbasan nº4 Tlf 876645373 ( El PVP indicado no incluye impuestos ni gastos de Escritura ).', 5, '876871503', 'p18.png'),
(20, 'Piso en Fernando el Catolico, Universidad', 2, 243500, 'Ubicado en una zona privilegiada de Zaragoza, con todas las comodidades a pie de calle, tranvía, comercios, universidad, zona de restauración, parques, siendo una estupenda zona para la familia.  Se vende en Fernando el Católico, un inmejorable piso de unos 100 metros útiles exterior, en la finca se cuenta con portero y ascensor, con calefacción de gas individual.  El piso cuenta con una detallada y esmerada reforma total con primeras calidades: suelos, electricidad, aire acondicionado, calefacción radiadores, puertas, alicatados, techos y ventanas etc. por lo tanto se entrega terminado a falta de mobiliario de cocina y lavamanos en baños, esto pensando en el gusto de cada cliente para la elección de los mismos.  La reforma está hecha con detalles visuales y materiales de primera calidad, el techo de pladur está hecho con láminas contra agua y aislamiento de lana de roca, protegiéndolo de cualquier tipo de humedades y resistencia térmica, las puertas de los baños son correderas para darle mayor amplitud al espacio interior, suelos y paredes de cerámica de gama alta e instalada con especialistas reformistas.  El piso está distribuido en salón comedor, dos dormitorios, el principal con vestidor cerrado amplio con ventana, dos baños, cocina con galería y balcón en el salón.  Luz natural en todos sus ambientes, con hermosas vistas del Paseo Fernando EL Católico.  La situación es extraordinaria junto al parque grande, hospitales, transporte tranvía y autobús así como zona de comercio de proximidad.', 3, '876872807', 'p12.png'),
(21, 'Piso en calle San Antonio María Claret', 2, 115000, 'Piso en venta Zona Universidad, San Francisco, Calle San Antonio Maria Claret, son 60 metros muy bien distribuidos en dos dormitorios con armarios empotrados, un cuarto de baño completo, Salón Abierto con Cocina Integrada totalmente equipada y de actualidad, con electrodomésticos de gama alta (Horno, Microondas, lavadora, lavavajillas, frigorífico, placa de inducción y campana extractora), Desde el Salón se accede a una Terraza Privada de 15m2. El piso es un Bajo que da a un patio interior Privado: mucha luz y muy Tranquilo, Llámenos para ir a ver esta Gran Ocasión llame a DonPiso Calle Mariano Barbasan nº4 Tlf 876645373 ( El PVP indicado no incluye impuestos ni gastos de Escritura ).', 3, '876871503', 'p10.png'),
(22, 'Ático en Corona de Aragón, 21-23', 3, 370000, 'El ático dispone de una gran entrada llena de luz y armario vestidor que dan paso a tres dormitorios exteriores con grandes ventanales y enormes armarios empotrados. El dormitorio principal sin lugar a dudas para soñar.. . un gran baño integrado en el dormitorio de ensueño con bañera de hidromasaje y jacuzzi y orientada a una bonita terraza. Un segundo baño completo en vivienda también con ducha de hidromasaje y un rincón de lavado con lavadora y secadora. Cocina totalmente equipada con placa, horno, y microondas Llegamos a un salón de 40m2 decorado a dos alturas y bonita chimenea con techos abuhardillados y aislados térmicamente. El salón tiene salida directa a una preciosa terraza con unas vistas increíbles y barbacoa para disfrutar en familia. Calefacción central, suelos parquet, aire acondicionado, ventanas climalit con aislamientos, puertas lacadas. Paredes y techos con aislante térmico. Instalación eléctrica y fontanería nuevas, paredes alisadas y puerta de entrada acorazada. Acceso directo desde la calle para movilidad reducida. ! Sin comisiones al comprador! Plaza de garaje opcional.', 2, '876275732', 'p16.png'),
(23, 'Piso en Plaza San Francisco, 8', 4, 450000, '160 m2 de Superficie Construida. 140 m2 de Superficie Útil.  4 Dormitorios:  - Dormitorio Principal de 15 m2 de Superficie Útil, con Baño en ', 6, '876274695', 'p13.png'),
(24, 'Piso en San Juan de la Cruz, 25', 4, 338500, 'Piso de lujo, completamente reformado con materiales de alto standing. Totalmente exterior, esta céntrica vivienda ubicada en un punto estratégico disfruta de privilegiadas vistas y de luz durante todo el día.  112 metros útiles en edificio señorial redistribuidos por un despacho de arquitectos de reconocido prestigio para dar un diseño más actual al espacio. Amplio salón de 26 metros, cocina totalmente equipada con electrodomésticos integrados de gama alta y gran servicio se ubica justo enfrente y cuenta con más de 9 metros, 4 dormitorios tres de ellos dobles y uno individual, dos baños completos uno de ellos en el dormitorio principal tipo suite, distribuidor y terraza en patio interior. Todos los dormitorios disponen de amplios armarios empotrados totalmente vestidos en su interior realizados con materiales de máxima calidad, además el vestíbulo tiene armario gabanero. Puntos de luz y aire acondicionado por conductos en toda la vivienda.  Plaza de parking exterior y dos trasteros uno de 9 m2 y otro de 5 m2 incluidos en el importe.', 6, '658987456', 'p14.png'),
(25, 'Piso en calle Luis Antonio Oro Giral, Universidad', 4, 139000, 'Universidad - Se vende piso exterior muy luminoso en planta tercera con ascensor, 90 metros, junto a la Universidad, a calle Bretón y al Edificio Torresol, ( el ascensor, se está acabando de instalar en estos momentos). Cuatro habitaciones amplias. Cocina-comedor de buen tamaño con ventana exterior y trastero. Calefacción individual a gas, pocos gastos de comunidad. - Se halla en buen estado. Piso perfecto para comprar y alquilar, con buena rentabilidad. Negociables los muebles, equipados para estudiantes.', 4, '876872875', 'p9.png'),
(26, 'Piso en venta en Fernando el Catolico, 33', 5, 299500, 'El Ático Azul HOMES te presenta este estupendo piso de 5 habitaciones, techos altos y en una zona inmejorable de Zaragoza, en Fernando el Católico 33, es un segundo piso con altura de cuarto, ya que tiene entresuelo y principal, junto a Pza San Francisco. Cerca de colegios, universidad, parques, comercios, y a escasos metros de la parada de tranvía. - Te presentamos además imágenes 3D de cómo podría quedar tras una reforma de diseño (con presupuesto incluido, pregúntanos) - Ideal también para inversores como piso de alquiler de estudiantes (consúltanos pequeña reforma planteada para crear un segundo baño sin mucho coste).       170 m² construidos, 155 m² útiles     5 habitaciones     1 baño     Terraza     Balcón     Segunda mano/para reformar     Armarios empotrados     Orientación norte, sur, este, oeste     Calefacción individual     Certificación energética: en trámite. Planta 2ª exterior, con ascensor', 3, '654782156', 'p11.png'),
(34, 'Piso a ver sis ale foto', 23, 68, '987654987', 65, '987654321', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nick` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape1` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape2` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(10) UNSIGNED NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nick`, `nombre`, `ape1`, `ape2`, `edad`, `correo`, `telefono`, `contra`) VALUES
(16, 'juan', 'Juan', 'Giron', 'Poves', 21, 'juan@gmail.com', '654987654', '654321');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nick` (`nick`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
