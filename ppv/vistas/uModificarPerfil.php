<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Modificar usuario </title>
    </head>

    <body>

        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <?php menuUser(); ?>
            <div class="contenedoradmin">
                <a href="uCambiarPassword.php">Cambiar contraseña</a>
                <div class="sign-up">
                    <div class="form">
                        <form action="../controlador/vuModificarPerfil.php" method="post">
                            <label>
                                <span> Nick </span>
                                <input type="text" name="nick" id="nick" value="<?php echo $_SESSION['nick'] ?>" required>
                            </label>
                            <label>
                                <span>Nombre</span>
                                <input type="text" name="nombre" id="nombre" value="<?php echo $_SESSION['nombre'] ?>" required>
                            </label>
                            <label>
                                <span>Primer apellido</span>
                                <input type="text" name="ape1" id="ape1" value="<?php echo $_SESSION['ape1'] ?>" required>
                            </label>
                            <label>
                                <span>Segundo apellido</span>
                                <input type="text" name="ape2" id="ape2" value="<?php echo $_SESSION['ape2'] ?>" required>
                            </label>
                            <label>
                                <span>Edad</span>
                                <input type="number" name="edad" id="edad" value="<?php echo $_SESSION['edad'] ?>" required>
                            </label>
                            <label>
                                <span>Telefono </span>
                                <input type="text" name="telefono" id="telefono" value="<?php echo $_SESSION['telefono'] ?>" required>
                            </label>
                            <label>
                                <span>Email</span>
                                <input type="email" name="email" id="correo" value="<?php echo $_SESSION['correo'] ?>" required>
                            </label>
                            <button class="submit" type="submit"> Modificar </button>
                        </form>
                    </div>
                </div>
            </div>
        <?php
    } else {
        echo "Usted no es usuario, por lo que no puede entrar en la página.";
    }
        ?>
    </body>
    </html>