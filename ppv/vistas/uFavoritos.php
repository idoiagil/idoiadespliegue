<?php
session_start();
if (isset($_SESSION['nick'])) {
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Mensajes </title>
    </head>

    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarUser(); ?>
        <div class="bd">
            <?php menuUser(); ?>
            <div class="contenedoradmin">
                <table style="width: 100%; text-align:center;">
                    <thead style="margin:10px; padding: 10px; background-color: #b1b1b1" ;>
                        <tr>
                            <th> Piso </th>
                            <th> Ver </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include("../modelo/usuario.php");
                        $usuario = new Usuario();
                        $usuario->muestrameFavoritosTabla($_SESSION['nick']);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php
} else {
    echo "Usted no es usuario, por lo que no puede entrar en la página.";
}
    ?>
    </body>
    </html>