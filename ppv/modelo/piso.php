<?php
include_once ('conexion_bbdd.php');

class Piso {


    private $conexion;
    private $conectar;

    public function __construct() {
        $this->conectar = new conectar();
        $this-> conexion = $this->conectar->getconection();
    }


    function nuevoPiso($titulo, $habitaciones, $precio, $descripcion, $distancia, $telefono, $nombreImg) {

        $sql = ("insert into `pisos`(titulo, habitaciones, precio, descripcion, distancia, telefono, foto) values (?, ?, ?, ?, ?, ?, ?)" );

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('siisiss', $titulo, $habitaciones, $precio, $descripcion, $distancia, $telefono, $nombreImg);

        $stmt->execute();

        $stmt->close();

        return true;
    }

    function mostrarPisosSelect() {

        $sql = "SELECT * FROM `pisos`";
        $result = $this->conexion->query($sql);
        $fow = $result->fetch_assoc();

        do {
            echo "<option>" . $fow ['titulo'] . "</option>";
        } while ($fow = $result->fetch_assoc());

    }

    function crearSesionPiso($pisoElegido) {


        $sql = "SELECT * FROM `pisos` WHERE titulo = '$pisoElegido'";

        $result = $this->conexion->query($sql);

        $row = $result->fetch_array();

        if ($result->num_rows > 0) {
            $_SESSION['titulo'] = $row['titulo'];
            $_SESSION['habitaciones'] = $row['habitaciones'];
            $_SESSION['precio'] = $row['precio'];
            $_SESSION['descripcion'] = $row['descripcion'];
            $_SESSION['distancia'] = $row['distancia'];
            $_SESSION['telefono'] = $row['telefono'];
            $_SESSION['foto'] = $row['foto'];
            return true;
        } else {
            return false;
        }
    }

    function muestramePiso($pisoElegido) {


        $sql = "SELECT * FROM `pisos` WHERE id = '$pisoElegido'";

        $result = $this->conexion->query($sql);

        $row = $result->fetch_array();

        if ($result->num_rows > 0) {
            $_SESSION['titulo'] = $row['titulo'];
            $_SESSION['habitaciones'] = $row['habitaciones'];
            $_SESSION['precio'] = $row['precio'];
            $_SESSION['descripcion'] = $row['descripcion'];
            $_SESSION['distancia'] = $row['distancia'];
            $_SESSION['telefono'] = $row['telefono'];
            $_SESSION['foto'] = $row['foto'];
            return true;
        } else {
            return false;
        }
    }

    function modPiso($titulo, $habitaciones, $precio, $descripcion, $distancia, $telefono, $nombreimg) {
       
        session_start();
        $titulo1 = $_SESSION['titulo'];

        $cst = $this->conexion->query("SELECT id FROM `pisos` WHERE titulo = '$titulo1'");
        $row = $cst->fetch_array();
        $idPiso = $row['id'];

        $sql = ("update `pisos` set titulo = ?, habitaciones = ?, precio = ?, descripcion = ?, distancia = ?, telefono = ?, foto = ? where id = ?");

        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('siisissi',$titulo, $habitaciones, $precio, $descripcion, $distancia, $telefono, $nombreimg, $idPiso);



        $stmt->execute();
       
        if(mysqli_affected_rows($this->conexion) > 0) {
            $_SESSION['titulo'] = $titulo;
            $_SESSION['habitaciones'] = $habitaciones;
            $_SESSION['precio'] = $precio;
            $_SESSION['descripcion'] = $descripcion;
            $_SESSION['distancia'] = $distancia;
            $_SESSION['telefono'] = $telefono;
            $_SESSION['foto'] = $nombreimg;
            $stmt->close();
            return true ;
        } else {
            $stmt->close();
            return false;
        }


    }

    function mostrarPisos() {

            $sql = "SELECT * FROM `pisos`";
            $result = $this->conexion->query($sql);
            $fow = $result->fetch_assoc();
            $contador = 0;
            do {
                echo "<div class='col-sm-6 col-12'>";
                echo "<div class='card' style='top: 15px;'>";
                echo "<img class='card-img-top' src='images/400X200.gif' alt='Card image cap'>";
                echo "<div class='card-body'>";
                echo "<h5 class='card-tittle'>".$fow['titulo'] ."</h5>";
                echo "<p class='card-text'>";
                    echo "Habitaciones: " . $fow['habitaciones'] . "<br>";
                    echo "Precio: " . $fow['precio'] . "<br>";
                    echo "Distancia al Montessori: " . $fow['distancia'] . "<br>";
                    echo "Telefono: " . $fow['telefono'] . "<br>";
                    echo "Foto: " . $fow['foto'] . "<br>";    
                echo "</p>";

                echo "<form action='vistas/user/descripcionPiso.php' method='post'>";
                echo "<input type='hidden' value='" . $fow['id'] . "' name='id'>";
                echo "<button class='submit' type='submit' style='color: #fff; background-color: #007bff; border-color: #007bff; font-weight:400; color:#212529;text-align: center; border: 1px solid transparent; border-radius: 0.25rem;'> Descripción </button>";
                echo "</form>";
                echo "</div></div></div>";
                $contador ++;
                if ($contador % 2 == 0) {
                    echo "</div><div class='row'>";
                }
            } while ($fow = $result->fetch_assoc());

    }




}