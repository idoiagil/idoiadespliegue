<?php
include_once('conexion_bbdd.php');

class Mensaje
{

    private $conexion;
    private $conectar;

    public function __construct()
    {
        $this->conectar = new conectar();
        $this->conexion = $this->conectar->getconection();
    }

    function enviarMensaje($titulo, $de, $para, $mensaje, $asunto)
    {


        $sql = ("insert into `mensajes`(titulo, de, para, mensaje, asunto) values (?, ?, ?, ?, ?)");

        $stmt = $this->conexion->prepare($sql);

        $stmt->bind_param('sssss', $titulo, $de, $para, $mensaje, $asunto);

        $stmt->execute();

        $stmt->close();

        return true;
    }


    function aMuestrameMensajesTabla($para)
    {
        $sql = "SELECT * FROM `mensajes` WHERE para = '$para' ORDER BY id DESC";
        $result = $this->conexion->query($sql);
        $fow = $result->fetch_assoc();
        if ($result->num_rows == 0) {
        } else {
            do {
                echo "<form action='aVerMensaje.php' method='post'>";
                echo "<tr>";
                echo "<input type='hidden' name='id' value='" . $fow['id'] . "'>";
                echo "<input type='hidden' name='leido' value='" . $fow['leido'] . "'>";

                if ($fow['leido'] == 0) {
                    echo "<td></td>";
                } else {
                    echo "<td> ✓ </td>";
                }
                echo "<input type='hidden' name='de' value='" . $fow['de'] . "'>";
                echo "<td>" . $fow['de'] . "</td>";
                echo "<input type='hidden' name='asunto' value='" . $fow['asunto'] . "'>";
                echo "<td><u><b><input style='width= 50%;' type='submit' value='" . $fow['asunto'] . "' style='border: 0px;' ><u><b></td>";
                echo "<input type='hidden' name='para' value='" . $fow['para'] . "'>";
                echo "<input type='hidden' name='titulo' value='" . $fow['titulo'] . "'>";
                echo "<input type='hidden' name='mensaje' value='" . $fow['mensaje'] . "'>";
                echo "</tr>";
                echo "</form>";
            } while ($fow = $result->fetch_assoc());
        }
    }

    
    function uMuestrameMensajesTabla($para)
    {
        $sql = "SELECT * FROM `mensajes` WHERE para = '$para' ORDER BY id DESC";
        $result = $this->conexion->query($sql);
        $fow = $result->fetch_assoc();
        if ($result->num_rows == 0) {
        } else {
            do {
                echo "<form action='uVerMensaje.php' method='post'>";
                echo "<tr>";
                echo "<input type='hidden' name='id' value='" . $fow['id'] . "'>";
                echo "<input type='hidden' name='leido' value='" . $fow['leido'] . "'>";

                if ($fow['leido'] == 0) {
                    echo "<td></td>";
                } else {
                    echo "<td> ✓ </td>";
                }
                echo "<input type='hidden' name='de' value='" . $fow['de'] . "'>";
                echo "<td>" . $fow['de'] . "</td>";
                echo "<input type='hidden' name='asunto' value='" . $fow['asunto'] . "'>";
                echo "<td><u><b><input style='width= 50%;' type='submit' value='" . $fow['asunto'] . "' style='border: 0px;' ><u><b></td>";
                echo "<input type='hidden' name='para' value='" . $fow['para'] . "'>";
                echo "<input type='hidden' name='titulo' value='" . $fow['titulo'] . "'>";
                echo "<input type='hidden' name='mensaje' value='" . $fow['mensaje'] . "'>";
                echo "</tr>";
                echo "</form>";
            } while ($fow = $result->fetch_assoc());
        }
    }


    function leido($leido, $id)
    {
        $sql = ("update `mensajes` set leido = ? where id = ?");
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('ii', $leido, $id);
        $stmt->execute();
    }

    function eliminarMensaje($id)
    {
        $sql = ("delete from `mensajes` where id = '$id' ");
        $stmt = $this->conexion->prepare($sql);
        $stmt->bind_param('s', $id);
        $stmt->execute();
        if (mysqli_affected_rows($this->conexion) > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }
}
