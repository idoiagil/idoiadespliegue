<?php
session_start();
if (isset($_SESSION['adm'])) {
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../css/bootstrap-4.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estilos.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Mensajes </title>
    </head>
    <body>
        <?php include("zlibreria.php"); ?>
        <?php navbarAdmin(); ?>
        <div class="bd">
           <?php menuAdmin(); ?>
            <div class="contenedoradmin" style="position:relative; top: 15px; left:15px;">
                <?php
                $de = $_POST['de'];
                $para = $_POST['para'];
                $titulo = $_POST['titulo'];
                $asunto = $_POST['asunto'];
                $mensaje = $_POST['mensaje'];
                $leido = 1;
                $id = $_POST['id'];
                echo "<h5 style='text-align: left'><b>Asunto:</b> " . $asunto . "</h5><br>";
                echo "<h5><b>De:</b> " . $de . "</h5><br>";
                echo "<h5><b>Piso referido:</b> " . $titulo . "</h5><br>";
                echo "<h5><b>Mensaje:</b></h5> <br>" . $mensaje . "<br>";
                include("../modelo/mensaje.php");
                $mensajeO = new Mensaje();
                $mensajeO->leido($leido, $id)
                ?>

                <form action="aResponderMensaje.php" method="post">
                    <?php
                    echo "<input type='hidden' name='titulo' value='" . $titulo . "'>";
                    echo "<input type='hidden' name='para' value='" . $de . "'>";
                    echo "<input type='hidden' name='de' value='" . $para . "'>";
                    echo "<input type='hidden' name='asunto' value='" . $asunto . "'>";
                    ?>
                    <input type="submit" name="submit" value="Responder">
                    <a href='aMensajes.php'> Volver </a>
                </form>
                <form action="../controlador/vaEliminarMensaje.php" method="post">
                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                    <input type="submit" value="eliminar">
                </form>
            </div>
        </div>
    <?php
            } else {
                echo "Usted no es administrador, por lo que no puede entrar en la página.";
            }
    ?>
    </body>
    </html>